
(in-package :gen-prog)

(defmacro deflib ((lib) &body clauses)
  `(progn
     ,@(iter (for (bind ty impl) in clauses)
             (collect `(defparameter ,bind ,impl)))
     (defparameter ,lib
       (list ,@(iter (for (bind ty impl) in clauses)
                     (collect `(cons ',bind (mksc ,ty))))))))

(deflib (*lib/i*)
  (*c/1i* (mkty int) 1)
  (*f/1+i* (mkfn (mkty int) (mkty int)) #'1+)
  (*f/1-i* (mkfn (mkty int) (mkty int)) #'1-)
  (*f/+i* (mkfn (mkty int) (mkfn (mkty int) (mkty int)))
          (lambda (x) (lambda (y) (+ x y))))
  (*f/-i* (mkfn (mkty int) (mkfn (mkty int) (mkty int)))
          (lambda (x) (lambda (y) (- x y))))
  (*f/*i* (mkfn (mkty int) (mkfn (mkty int) (mkty int)))
          (lambda (x) (lambda (y) (* x y))))
  (*f//i* (mkfn (mkty int) (mkfn (mkty int-nonzero) (mkty int)))
          (lambda (x) (lambda (y) (truncate x y))))
  (*f/zeropi* (mkfn (mkty int) (mkty bool)) #'zerop)
  (*if/nonzeroi* (mkfn (mkfn (mkty int-nonzero) (mkva a)) (mkfn (mkfn (mkty int) (mkva a)) (mkfn (mkty int) (mkva a))))
                 (lambda (true-f) (lambda (false-f) (lambda (value) (funcall (if (zerop value) false-f true-f) value)))))
  )

(deflib (*combinators*)
  (*f/id* (mkfn (mkva a) (mkva a)) #'identity)
  (*f/flip* (mkfn (mkfn (mkva a) (mkfn (mkva b) (mkva c))) (mkfn (mkva b) (mkfn (mkva a) (mkva c))))
            (lambda (f) (lambda (b) (lambda (a) (funcall (funcall f a) b)))))
  (*f/compose* (mkfn (mkfn (mkva b) (mkva c)) (mkfn (mkfn (mkva a) (mkva b)) (mkfn (mkva a) (mkva c))))
               (lambda (g) (lambda (f) (lambda (x) (funcall g (funcall f x))))))
  )

(defun stdlib ()
  (make-instance 'ty-env :maps (append *lib/i* *combinators*)))



