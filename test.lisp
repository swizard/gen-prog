
(in-package :gen-prog)

(defun counter-k (gen)
  (let ((counter 0))
    (multiple-value-call
        (curry #'iter-k (lambda (value k) (declare (ignore value)) (incf counter) (funcall k)) (f_% counter))
      (funcall gen))))

(defun simulate (arg-ty ret-ty &key (depth-lim 4) inline-p expr-cond (set-modif #'identity))
  (iter (with gen = (funcall (compose #'expr->sexp-gen
                                      (f_ (post-process-gen _ :inline-p inline-p))
                                      (f_ (filter-gen _ :value-cond expr-cond)))
                             (f_% (run-gen-k (stdlib) arg-ty ret-ty :depth-lim depth-lim :set-modif set-modif))))
        (with code+env = nil)
        (with kont = nil)
        (initially (multiple-value-setq (code+env kont) (funcall gen)))
        (while code+env)
        (for code = (car code+env))
        (for func = (eval code))
        (break "code: ~a~%values:~{~%f(~a) = ~a~}"
               code
               (iter (for i from 0 below 5) (collect i) (collect (funcall func i))))
        (multiple-value-setq (code+env kont) (funcall kont))))

(defgeneric contain-binding-p (name expr))
(defmethod contain-binding-p (name (expr expr)) nil)
(defmethod contain-binding-p (name (expr expr-var)) (eq (name expr) name))
(defmethod contain-binding-p (name (expr expr-app)) (or (contain-binding-p name (fn expr)) (contain-binding-p name (arg expr))))
(defmethod contain-binding-p (name (expr expr-abs)) (contain-binding-p name (body expr)))
(defmethod contain-binding-p (name (expr expr-let)) (or (contain-binding-p name (let-body expr)) (contain-binding-p name (body expr))))

(defun fitness-x^2+2x+1 (func)
  (iter (for x from -10 to 10)
        (for sample = (+ (* x x) (* 2 x) 1))
        (for value = (funcall func x))
        (for err = (- sample value))
        (summing (* err err) into sq-err)
        (finally (return (if (zerop sq-err) 1.0 (/ 1.0 sq-err))))))

(defun make-test-population ()
  (let* ((scheme (make-instance 'program-scheme
                                :init-depth 4
                                :arg-ty (mkty int)
                                :ret-ty (mkty int)
                                :fitness #'fitness-x^2+2x+1))
         (pop (make-instance 'population
                             :program-scheme scheme
                             :population-size 500
                             :cross-count 300
                             :survivors-count 100
                             :mutate-crossed-count 50
                             :mutate-survived-count 20)))
    (spawn-population pop)))

(defun run-ga (&key (population (make-test-population)))
  population)
