
(in-package :gen-prog)

;; expressions
(defclass expr () ())

(defclass expr-var (expr)
  ((name :initarg :name :reader name)))

(defclass expr-lit (expr)
  ((literal :initarg :literal :reader literal)))

(defclass expr-app (expr)
  ((fn :initarg :fn :reader fn)
   (arg :initarg :arg :reader arg)))

(defclass expr-abs (expr)
  ((arg-bind :initarg :arg-bind :reader arg-bind)
   (body :initarg :body :reader body)))

(defclass expr-let (expr)
  ((let-bind :initarg :let-bind :reader let-bind)
   (let-body :initarg :let-body :reader let-body)
   (body :initarg :body :reader body)))

;; literals
(defclass lit ()
  ((value :initarg :value :reader value)))

(defclass lit-int (lit) ())
(defclass lit-bool (lit) ())

;; types
(defclass ty () ())

(defclass ty-var (ty)
  ((name :initarg :name :reader name)))

(defclass ty-int (ty) ())
(defclass ty-int-nonzero (ty) ())
(defclass ty-bool (ty) ())

(defclass ty-fn (ty)
  ((arg :initarg :arg :reader arg)
   (ret :initarg :ret :reader ret)))

;; type scheme
(defclass ty-scheme ()
  ((vars :initarg :vars :reader vars)
   (ty :initarg :ty :reader ty)))

(defgeneric ty-applicable (ty))
(defmethod ty-applicable ((ty ty)) nil)
(defmethod ty-applicable ((ty ty-fn)) t)
(defmethod ty-applicable ((ty ty-scheme)) (ty-applicable (ty ty)))

;; type environment
(defclass ty-env ()
  ((maps :initarg :maps :reader maps)))

(defgeneric var-remove (env var))
(defmethod var-remove ((env ty-env) var)
  (make-instance 'ty-env :maps (remove var (maps env) :key #'car)))

;; free type variables
(defgeneric ftv (ty))

(defmethod ftv ((ty ty-var)) (list (name ty)))
(defmethod ftv ((ty ty-int)) nil)
(defmethod ftv ((ty ty-int-nonzero)) nil)
(defmethod ftv ((ty ty-bool)) nil)
(defmethod ftv ((ty ty-fn)) (union (ftv (arg ty)) (ftv (ret ty))))
(defmethod ftv ((ty ty-scheme)) (set-difference (ftv (ty ty)) (vars ty)))
(defmethod ftv ((tys list)) (reduce #'union (mapcar #'ftv tys) :initial-value nil))
(defmethod ftv ((ty ty-env)) (ftv (mapcar #'cdr (maps ty))))

;; substitute
(defgeneric ty-apply (subst ty))

(defmethod ty-apply (subst (ty ty)) ty)
  
(defmethod ty-apply (subst (ty ty-var))
  (let ((subst-ty (cdr (assoc (name ty) subst))))
    (if subst-ty subst-ty ty)))

(defmethod ty-apply (subst (ty ty-fn))
  (make-instance 'ty-fn :arg (ty-apply subst (arg ty)) :ret (ty-apply subst (ret ty))))

(defmethod ty-apply (subst (ty ty-scheme))
  (make-instance 'ty-scheme
                 :vars (vars ty)
                 :ty (ty-apply (set-difference subst (vars ty)) (ty ty))))

(defmethod ty-apply (subst (tys list))
  (mapcar (f_ (ty-apply subst _)) tys))

(defmethod ty-apply (subst (ty ty-env))
  (make-instance 'ty-env :maps (mapcar (f_ (cons (car _) (ty-apply subst (cdr _)))) (maps ty))))

(defun compose-subst (subst-a subst-b)
  (union (mapcar (f_ (cons (car _) (ty-apply subst-a (cdr _)))) subst-b)
         subst-a
         :key #'car))


(defgeneric generalize (env ty))
(defmethod generalize ((env ty-env) (ty ty))
  (make-instance 'ty-scheme :vars (set-difference (ftv ty) (ftv env)) :ty ty))

(defgeneric instantiate (ty))
(defmethod instantiate ((ty ty-scheme))
  (let ((table (mapcar (f_ (cons _ (make-instance 'ty-var :name (gensym)))) (vars ty))))
    (ty-apply table (ty ty))))

;; typecheck errors
(define-condition ty-unify-error (error)
  ((ty-a :initarg :ty-a :reader ty-a)
   (ty-b :initarg :ty-b :reader ty-b)))

(define-condition ty-var-bind (error)
  ((name :initarg :name :reader name)
   (ty :initarg :ty :reader ty)))


;; most general unifier
(defgeneric mgu (ty ty))

(defmethod mgu ((ty-a ty-fn) (ty-b ty-fn))
  (let* ((subst-arg (mgu (arg ty-a) (arg ty-b)))
         (subst-ret (mgu (ty-apply subst-arg (ret ty-a)) (ty-apply subst-arg (ret ty-b)))))
    (compose-subst subst-arg subst-ret)))

(defmethod mgu ((ty-a ty-var) (ty-b ty)) (var-bind (name ty-a) ty-b))
(defmethod mgu ((ty-a ty) (ty-b ty-var)) (var-bind (name ty-b) ty-a))

(defmethod mgu ((ty-a ty-int) (ty-b ty-int)) nil)
(defmethod mgu ((ty-a ty-int-nonzero) (ty-b ty-int-nonzero)) nil)
(defmethod mgu ((ty-a ty-bool) (ty-b ty-bool)) nil)

(defmethod mgu ((ty-a ty) (ty-b ty)) (error 'ty-unify-error :ty-a ty-a :ty-b ty-b))

(defun var-bind (name ty)
  (cond ((and (eq (type-of ty) 'ty-var) (eq name (name ty))) nil)
        ((find name (ftv ty)) (error 'ty-var-bind :name name :ty ty))
        (t (list (cons name ty)))))
  
;; types for literals
(defgeneric ti-lit (env lit))
(defmethod ti-lit ((env ty-env) (lit lit-int)) (values nil (make-instance 'ty-int)))
(defmethod ti-lit ((env ty-env) (lit lit-bool)) (values nil (make-instance 'ty-bool)))

;; type inference
(defgeneric infer-type (env expr))

(defmethod infer-type ((env ty-env) (var expr-var))
  (let ((bind (cdr (assoc (name var) (maps env)))))
    (unless bind
      (error "unbound variable: ~a" var))
    (values nil (instantiate bind))))

(defmethod infer-type ((env ty-env) (lit expr-lit)) (ti-lit env (literal lit)))

(defmethod infer-type ((env ty-env) (abstract expr-abs))
  (let* ((tv (make-instance 'ty-var :name (gensym)))
         (env-a (var-remove env (arg-bind abstract)))
         (env-b (make-instance 'ty-env
                               :maps (union (maps env-a)
                                            (list (cons (arg-bind abstract)
                                                        (make-instance 'ty-scheme :vars nil :ty tv)))))))
    (multiple-value-bind (subst-ret ty-ret)
        (infer-type env-b (body abstract))
      (values subst-ret (make-instance 'ty-fn :arg (ty-apply subst-ret tv) :ret ty-ret)))))

(defmethod infer-type ((env ty-env) (app expr-app))
  (let ((tv (make-instance 'ty-var :name (gensym))))
    (multiple-value-bind (subst-fn ty-fn) (infer-type env (fn app))
      (multiple-value-bind (subst-arg ty-arg) (infer-type (ty-apply subst-fn env) (arg app))
        (let ((subst-mgu (mgu (ty-apply subst-arg ty-fn)
                              (make-instance 'ty-fn :arg ty-arg :ret tv))))
          (values (compose-subst (compose-subst subst-mgu subst-arg) subst-fn)
                  (ty-apply subst-mgu tv)))))))

(defmethod infer-type ((env ty-env) (elet expr-let))
  (multiple-value-bind (subst-let-body ty-let-body)
      (infer-type env (let-body elet))
    (let* ((env-a (var-remove env (let-bind elet)))
           (ty-g (generalize (ty-apply subst-let-body env) ty-let-body))
           (env-b (make-instance 'ty-env :maps (cons (cons (let-bind elet) ty-g) (maps env-a)))))
      (multiple-value-bind (subst-body ty-body)
          (infer-type (ty-apply subst-let-body env-b) (body elet))
        (values (compose-subst subst-let-body subst-body) ty-body)))))

;; helpers

(defmacro mkty (type) `(make-instance ',(form-symbol 'ty- type)))
(defmacro mkfn (arg ret) `(make-instance 'ty-fn :arg ,arg :ret ,ret))
(defmacro mkva (name) `(make-instance 'ty-var :name ',name))
(defmacro mksc (type) (let ((ty (gensym))) `(let ((,ty ,type)) (make-instance 'ty-scheme :vars (ftv ,ty) :ty ,ty))))

(defmacro with-match ((ast proc) &rest clauses)
  (with-gensyms (assume form)
    `(flet ((,assume (,form) (return-from ,proc ,form)))
       (progn ,@(iter (for (pattern action) in clauses)
                      (collect `(if-match ,pattern ,ast (,assume ,action))))))))

(defun sexp->expr (code)
  (labels ((walk (ast)
             (with-match (ast walk)
               ((lambda (?proc-arg) ?proc-body)
                (make-instance 'expr-abs
                               :arg-bind ?proc-arg
                               :body (walk ?proc-body)))
               ((let ((?binding ?form)) ?body)
                (make-instance 'expr-let
                               :let-bind ?binding
                               :let-body (walk ?form)
                               :body (walk ?body)))
               ((funcall ?func ?arg)
                (make-instance 'expr-app
                               :fn (walk ?func)
                               :arg (walk ?arg)))
               (?atom
                (cond ((eq ?atom nil) (make-instance 'expr-lit :literal (make-instance 'lit-bool :value nil)))
                      ((eq ?atom t) (make-instance 'expr-lit :literal (make-instance 'lit-bool :value t)))
                      ((integerp ?atom) (make-instance 'expr-lit :literal (make-instance 'lit-int :value ?atom)))
                      ((symbolp ?atom) (make-instance 'expr-var :name ?atom))
                      (t (error "unknown atom: ~a" ?atom)))))))
    (walk code)))

(defgeneric expr->sexp (expr &key ignorable-p))
(defmethod expr->sexp ((expr expr-var) &key &allow-other-keys) (name expr))
(defmethod expr->sexp ((expr expr-lit) &key &allow-other-keys) (value (literal expr)))
(defmethod expr->sexp ((expr expr-app) &key &allow-other-keys) `(funcall ,(expr->sexp (fn expr)) ,(expr->sexp (arg expr))))
(defmethod expr->sexp ((expr expr-let) &key &allow-other-keys) `(let ((,(let-bind expr) ,(expr->sexp (let-body expr)))) ,(expr->sexp (body expr))))
(defmethod expr->sexp ((expr expr-abs) &key ignorable-p)
  `(lambda (,(arg-bind expr)) ,@(when ignorable-p `((declare (ignorable ,(arg-bind expr))))) ,(expr->sexp (body expr))))

(defgeneric dump-ty (ty))
(defmethod dump-ty ((ty ty)) (type-of ty))
(defmethod dump-ty ((ty ty-var)) (name ty))
(defmethod dump-ty ((ty ty-fn)) (list (dump-ty (arg ty)) '-> (dump-ty (ret ty))))

