
(in-package :gen-prog)

(defgeneric collect-bindings (table expr))
(defmethod collect-bindings (table (expr expr-var))
  (let ((bind (gethash (name expr) table)))
    (when bind
      (incf (car bind))))
  nil)
(defmethod collect-bindings (table (expr expr-lit)) nil)
(defmethod collect-bindings (table (expr expr-app))
  (collect-bindings table (fn expr))
  (collect-bindings table (arg expr)))
(defmethod collect-bindings (table (expr expr-abs))
  (collect-bindings table (body expr)))
(defmethod collect-bindings (table (expr expr-let))
  (setf (gethash (let-bind expr) table)
        (cons 0 (let-body expr)))
  (collect-bindings table (let-body expr))
  (collect-bindings table (body expr)))

(defun has-free-bindings (bindings-table)
  (iter (for (bind (count . form)) in-hashtable bindings-table)
        (when (zerop count)
          (return-from has-free-bindings t))))

(defgeneric inline-bindings (table expr))
(defmethod inline-bindings (table (expr expr-var))
  (let ((bind (gethash (name expr) table)))
    (if (and bind (= (car bind) 1))
        (inline-bindings table (cdr bind))
        expr)))
(defmethod inline-bindings (table (expr expr-lit)) expr)
(defmethod inline-bindings (table (expr expr-app))
  (make-instance 'expr-app
                 :fn (inline-bindings table (fn expr))
                 :arg (inline-bindings table (arg expr))))
(defmethod inline-bindings (table (expr expr-abs))
  (make-instance 'expr-abs
                 :arg-bind (arg-bind expr)
                 :body (inline-bindings table (body expr))))
(defmethod inline-bindings (table (expr expr-let))
  (let ((bind (gethash (let-bind expr) table)))
    (if (and bind (= (car bind) 1))
        (inline-bindings table (body expr))
        (make-instance 'expr-let
                       :let-bind (let-bind expr)
                       :let-body (inline-bindings table (let-body expr))
                       :body (inline-bindings table (body expr))))))

(defgeneric collect-active-bindings (expr))
(defmethod collect-active-bindings ((expr expr-var)) (list (name expr)))
(defmethod collect-active-bindings ((expr expr-lit)) nil)
(defmethod collect-active-bindings ((expr expr-app))
  (union (collect-active-bindings (fn expr)) (collect-active-bindings (arg expr))))
(defmethod collect-active-bindings ((expr expr-abs))
  (union (list (arg-bind expr)) (collect-active-bindings (body expr))))
(defmethod collect-active-bindings ((expr expr-let))
  (let ((body-bindings (collect-active-bindings (body expr))))
    (union (when (member (let-bind expr) body-bindings)
             (collect-active-bindings (let-body expr)))
           body-bindings)))

(defgeneric optimize-free-bindings (active-bindings expr))
(defmethod optimize-free-bindings (active-bindings (expr expr-var)) expr)
(defmethod optimize-free-bindings (active-bindings (expr expr-lit)) expr)
(defmethod optimize-free-bindings (active-bindings (expr expr-app))
  (make-instance 'expr-app
                 :fn (optimize-free-bindings active-bindings (fn expr))
                 :arg (optimize-free-bindings active-bindings (arg expr))))
(defmethod optimize-free-bindings (active-bindings (expr expr-abs))
  (make-instance 'expr-abs
                 :arg-bind (arg-bind expr)
                 :body (optimize-free-bindings active-bindings (body expr))))
(defmethod optimize-free-bindings (active-bindings (expr expr-let))
  (if (member (let-bind expr) active-bindings)
      (make-instance 'expr-let
                     :let-bind (let-bind expr)
                     :let-body (optimize-free-bindings active-bindings (let-body expr))
                     :body (optimize-free-bindings active-bindings (body expr)))
      (optimize-free-bindings active-bindings (body expr))))

(defgeneric restore-env (env expr))
(defmethod restore-env ((env ty-env) (expr expr)) env)

(defmethod restore-env ((env ty-env) (expr expr-abs))
  (multiple-value-bind (subst fn-ty) (infer-type env expr)
    (declare (ignore subst))
    (restore-env (make-instance 'ty-env :maps (cons (cons (arg-bind expr) (mksc (arg fn-ty))) (maps env)))
                 (body expr))))

(defmethod restore-env ((env ty-env) (expr expr-let))
  (multiple-value-bind (subst let-ty) (infer-type env (let-body expr))
    (declare (ignore subst))
    (restore-env (make-instance 'ty-env :maps (cons (cons (let-bind expr) (mksc let-ty)) (maps env)))
                 (body expr))))


(defgeneric rename-vars (expr &optional maps))
(defmethod rename-vars ((expr expr-var) &optional maps)
  (let ((rename (cdr (assoc (name expr) maps :test #'eq))))
    (if rename (make-instance 'expr-var :name rename) expr)))
(defmethod rename-vars ((expr expr-lit) &optional maps)
  (declare (ignore maps))
  expr)
(defmethod rename-vars ((expr expr-app) &optional maps)
  (make-instance 'expr-app :fn (rename-vars (fn expr) maps) :arg (rename-vars (arg expr) maps)))
(defmethod rename-vars ((expr expr-abs) &optional maps)
  (let ((rename (gensym)))
    (make-instance 'expr-abs
                   :arg-bind rename
                   :body (rename-vars (body expr) (cons (cons (arg-bind expr) rename) maps)))))
(defmethod rename-vars ((expr expr-let) &optional maps)
  (let ((rename (gensym)))
    (make-instance 'expr-let
                   :let-bind rename
                   :let-body (rename-vars (let-body expr) maps)
                   :body (rename-vars (body expr) (cons (cons (let-bind expr) rename) maps)))))

(defun build-expr+env (sexp)
  (let* ((expr (sexp->expr sexp))
         (env (restore-env (stdlib) expr)))
    (cons expr env)))

(defun filter-gen (k &key value-cond)
  (f_% (multiple-value-call
           (curry #'iter-k
                  (lambda (value k)
                    (if (or (not (functionp value-cond)) (funcall value-cond value))
                        (values value k)
                        (funcall k)))
                  (constantly nil))
         (funcall k))))

(defun expr->sexp-gen (k)
  (f_% (multiple-value-call
           (curry #'iter-k (lambda (expr+env k) (values (cons (expr->sexp (car expr+env)) (cdr expr+env)) k)) (constantly nil))
         (funcall k))))

(defun post-process-gen (k &key inline-p)
  (f_% (multiple-value-call
           (curry #'iter-k
                  (lambda (ast+env k)
                    (let ((ast (car ast+env))
                          (env (cdr ast+env))
                          (bindings-table (make-hash-table :test 'eq)))
                      (collect-bindings bindings-table ast)
                      (if (has-free-bindings bindings-table)
                          (funcall k)
                          (values (cons (if inline-p (inline-bindings bindings-table ast) ast) env) k))))
                  (constantly nil))
         (funcall k))))
