;;; -*- gen-prog main -*-

(defpackage #:gen-prog-asd
  (:use :cl :asdf))

(in-package #:gen-prog-asd)

(defsystem gen-prog
  :name "gen-prog"
  :version "0.1"
  :author "swizard"
  :depends-on (:iterate :metatilities :f-underscore)
  :components ((:file "package")
               (:file "hm" :depends-on ("package"))
               (:file "stdlib" :depends-on ("hm"))
               (:file "post" :depends-on ("hm"))
               (:file "gen-k" :depends-on ("post"))
               (:file "ga" :depends-on ("hm"))
               (:file "test" :depends-on ("stdlib" "gen-k"))
               ))

