
(in-package :gen-prog)

(defgeneric ast-bindings (env expr))
(defmethod ast-bindings ((env ty-env) (expr expr)) nil)
(defmethod ast-bindings ((env ty-env) (expr expr-let))
  (let ((bind+ty (assoc (let-bind expr) (maps env))))
    (assert bind+ty nil "unbound variable: ~a" (let-bind expr))
    (cons bind+ty (ast-bindings env (body expr)))))
(defmethod ast-bindings ((env ty-env) (expr expr-abs))
  (ast-bindings env (body expr)))

(defgeneric unificable-p (ty-a ty-b))
(defmethod unificable-p ((ty-a ty-scheme) (ty-b ty-scheme))
  (ignore-errors (progn (mgu (ty ty-a) (ty ty-b)) t)))

(defun crosspoints (bindings-a bindings-b)
  (iter outer
        (for (bind-a . ty-a) in bindings-a)
        (iter (for (bind-b . ty-b) in bindings-b)
              (when (unificable-p ty-a ty-b)
                (in outer (collect (list (cons bind-a ty-a) (cons bind-b ty-b))))))))

(defun possible-crosspoints-k (bindings-a bindings-b)
  (foreach-k (lambda (bind+ty-a k-a)
               (foreach-k (lambda (bind+ty-b k-b)
                            (if (unificable-p (cdr bind+ty-a) (cdr bind+ty-b))
                                (values (list bind+ty-a bind+ty-b) k-b)
                                (funcall k-b)))
                          (shuffle-list bindings-b)
                          k-a))
             (shuffle-list bindings-a)
             (constantly nil)))



(defgeneric traverse-expr-k (expr bindings-seen on-var on-let k))
(defmethod traverse-expr-k ((expr expr-var) bindings-seen on-var on-let k)
  (funcall on-var expr bindings-seen k))
(defmethod traverse-expr-k ((expr expr-app) bindings-seen on-var on-let k)
  (traverse-expr-k
   (fn expr) bindings-seen on-var on-let
   (lambda (fn-expr)
     (traverse-expr-k
      (arg expr) bindings-seen on-var on-let
      (lambda (arg-expr)
        (funcall k (make-instance 'expr-app :fn fn-expr :arg arg-expr)))))))
(defmethod traverse-expr-k ((expr expr-lit) bindings-seen on-var on-let k)
  (funcall k expr))
(defmethod traverse-expr-k ((expr expr-abs) bindings-seen on-var on-let k)
  (traverse-expr-k
   (body expr) (cons (arg-bind expr) bindings-seen) on-var on-let
   (lambda (body-expr)
     (funcall k (make-instance 'expr-abs :arg-bind (arg-bind expr) :body body-expr)))))
(defmethod traverse-expr-k ((expr expr-let) bindings-seen on-var on-let k)
  (traverse-expr-k
   (let-body expr)
   bindings-seen on-var on-let
   (lambda (let-body-expr)
     (traverse-expr-k
      (body expr)
      (cons (let-bind expr) bindings-seen) on-var on-let
      (lambda (body-expr)
        (funcall on-let
                 (make-instance 'expr-let :let-bind (let-bind expr) :let-body let-body-expr :body body-expr)
                 bindings-seen
                 k))))))

(defgeneric cross-exchange (expr-a bind-a expr-b bind-b))
(defmethod cross-exchange ((expr-a expr-abs) bind-a (expr-b expr-abs) bind-b)
  (declare (optimize (debug 3)))
  (let ((abs-arg-a (arg-bind expr-a))
        (abs-arg-b (arg-bind expr-b)))
    (traverse-expr-k
     expr-a '() (lambda (var-expr-a bindings k) (declare (ignore bindings)) (funcall k var-expr-a))
     (lambda (let-expr-a bindings let-k-a)
       (declare (ignore bindings))
       (if (eq (let-bind let-expr-a) bind-a)
           (traverse-expr-k
            expr-b '()
            (lambda (var-expr-a bindings k)
              (declare (ignore bindings))
              (funcall k (cond ((eq (name var-expr-a) abs-arg-b) (make-instance 'expr-var :name abs-arg-a))
                               ((eq (name var-expr-a) bind-b) (make-instance 'expr-var :name bind-a))
                               (t var-expr-a))))
            (lambda (let-expr-b bindings let-k-b)
              (declare (ignore bindings))
              (funcall let-k-b
                       (if (eq (let-bind let-expr-b) bind-b)
                           (make-instance 'expr-let
                                          :let-bind bind-a
                                          :let-body (let-body let-expr-b)
                                          :body (body let-expr-a))
                           let-expr-b)))
            (lambda (result-b) (funcall let-k-a (body result-b))))
           (funcall let-k-a let-expr-a)))
     #'identity)))

(defgeneric std-env+main-arg (expr std-env main-arg-ty))
(defmethod std-env+main-arg ((expr expr-abs) (std-env ty-env) (main-arg-ty ty-scheme))
  (make-instance 'ty-env :maps (cons (cons (arg-bind expr) main-arg-ty) (maps std-env))))

(defun cross-k (std-env expr+env-a expr+env-b)
  (declare (optimize (debug 3)))
  (let ((bindings-a (ast-bindings (cdr expr+env-a) (car expr+env-a)))
        (bindings-b (ast-bindings (cdr expr+env-b) (car expr+env-b)))
        (main-arg-ty (cdr (assoc (arg-bind (car expr+env-a)) (maps (cdr expr+env-a))))))
    (multiple-value-call
        (curry #'iter-k
               (lambda (crosspoint k)
                 (let ((merged-expr (cross-exchange (car expr+env-a)
                                                    (car (first crosspoint))
                                                    (car expr+env-b)
                                                    (car (second crosspoint)))))
                   (if (maybe-infer-type (std-env+main-arg merged-expr std-env main-arg-ty) (body merged-expr))
                       (let ((final-expr (rename-vars (optimize-free-bindings (collect-active-bindings merged-expr) merged-expr))))
                         (values (cons final-expr (restore-env (std-env+main-arg final-expr std-env main-arg-ty) (body final-expr))) k))
                       (funcall k))))
               (constantly nil))
      (possible-crosspoints-k bindings-a bindings-b))))

(defgeneric traverse-leaves-collect (expr))
(defmethod traverse-leaves-collect ((expr expr-abs))
  (multiple-value-bind (expr bindings k)
      (traverse-expr-k expr '() #'values (lambda (expr bindings k) (declare (ignore bindings)) (funcall k expr)) #'identity)
    (iter (while k)
          (collect (cons (cons expr bindings)
                         (labels ((finish-rec (kont replace-expr)
                                    (multiple-value-bind (expr bindings k) (funcall kont replace-expr)
                                      (if (and bindings k)
                                          (finish-rec k expr)
                                          expr))))
                           (let ((kont k)) 
                             (curry #'finish-rec kont)))))
          (multiple-value-setq (expr bindings k) (funcall k expr)))))

(defun mutate-k (std-env expr+env)
  (declare (optimize (debug 3)))
  (let* ((expr (car expr+env))
         (env (cdr expr+env))
         (main-arg-ty (cdr (assoc (arg-bind (car expr+env)) (maps (cdr expr+env)))))
         (leaves (traverse-leaves-collect expr)))
    (foreach-k
     (lambda (leave+seen next-leave-k)
       (let* ((leave-expr (caar leave+seen))
              (leave-maps (reduce (lambda (env-acc seen-binding) (cons (find seen-binding (maps env) :key #'car) env-acc))
                                  (cdar leave+seen)
                                  :initial-value (maps std-env)))
              (leave-ty (cdr (assoc (name leave-expr) leave-maps)))
              (builder (cdr leave+seen)))
         (if leave-ty
             (let* ((rest-maps (remove (name leave-expr) leave-maps :test #'eq :key #'car))
                    (valid-replaces (remove-if-not (curry #'unificable-p leave-ty) rest-maps :key #'cdr)))
               (foreach-k
                (lambda (replace next-replace-k)
                  (let ((mutated-expr (funcall builder (make-instance 'expr-var :name (car replace)))))
                    (if (maybe-infer-type (std-env+main-arg mutated-expr std-env main-arg-ty) (body mutated-expr))
                        (let ((final-expr (rename-vars (optimize-free-bindings (collect-active-bindings mutated-expr) mutated-expr))))
                          (values (cons final-expr (restore-env (std-env+main-arg final-expr std-env main-arg-ty) (body final-expr))) next-replace-k))
                        (funcall next-replace-k))))
                (shuffle-list valid-replaces)
                next-leave-k))
             (funcall next-leave-k))))
     (shuffle-list leaves)
     (constantly nil))))

(defgeneric expr-depth (expr))
(defmethod expr-depth ((expr expr-var)) 1)
(defmethod expr-depth ((expr expr-lit)) 1)
(defmethod expr-depth ((expr expr-app)) (+ 1 (expr-depth (fn expr)) (expr-depth (arg expr))))
(defmethod expr-depth ((expr expr-abs)) (+ 1 (expr-depth (body expr))))
(defmethod expr-depth ((expr expr-let)) (+ 1 (expr-depth (let-body expr)) (expr-depth (body expr))))

(defclass program-scheme ()
  ((init-depth :initarg :init-depth :reader init-depth)
   (arg-ty :initarg :arg-ty :reader arg-ty)
   (ret-ty :initarg :ret-ty :reader ret-ty)
   (fitness :initarg :fitness :reader fitness)))

(defclass program ()
  ((code :initarg :code :reader code)
   (env :initarg :env :reader env)
   (spawn-generation :initarg :spawn-generation :reader spawn-generation)
   (fitness-value :accessor fitness-value)))

(defgeneric program< (p-a p-b))
(defmethod program< ((p-a program) (p-b program))
  (or (> (fitness-value p-a) (fitness-value p-b))
      (and (<= (abs (- (fitness-value p-a) (fitness-value p-b))) single-float-epsilon)
           (< (expr-depth (code p-a)) (expr-depth (code p-b))))))

(defmethod initialize-instance :after ((p program) &key fitness &allow-other-keys)
  (let ((compiled-prog (eval (expr->sexp (code p) :ignorable-p t))))
    (setf (fitness-value p) (funcall fitness compiled-prog))
    nil))

(defclass population ()
  ((scheme :initarg :program-scheme :type program-scheme :reader scheme)
   (std-env :initarg :std-env :initform (stdlib) :reader std-env)
   (pop-size :initarg :population-size :reader pop-size)
   (crs-count :initarg :cross-count :reader crs-count)
   (srv-count :initarg :survivors-count :reader srv-count)
   (mut-crs-count :initarg :mutate-crossed-count :reader mut-crs-count)
   (mut-srv-count :initarg :mutate-survived-count :reader mut-srv-count)
   (generation :initform 0 :accessor generation)
   (individuals :initform (make-array 0 :fill-pointer 0) :accessor individuals)))

(defun gen/add (generator pop individuals)
  (multiple-value-bind (code+env k) (funcall generator)
    (declare (ignore k))
    (when code+env
      (vector-push-extend (make-instance 'program
                                         :code (car code+env)
                                         :env (cdr code+env)
                                         :spawn-generation (generation pop)
                                         :fitness (fitness (scheme pop)))
                          individuals)
      t)))

(defun spawn-gen (pop)
  (post-process-gen
   (f_% (run-gen-k (std-env pop)
                   (arg-ty (scheme pop))
                   (ret-ty (scheme pop))
                   :depth-lim (init-depth (scheme pop))
                   :set-modif #'shuffle-list))))

(defmethod spawn-population ((pop population))
  (iter (while (< (length (individuals pop)) (pop-size pop)))
        (gen/add (spawn-gen pop) pop (individuals pop)))
  (sort (individuals pop) #'program<)
  pop)

(defmethod next-population ((pop population))
  (declare (optimize (debug 3)))
  (let ((next-individuals (make-array 0 :fill-pointer 0)))
    (incf (generation pop))
    ;; crossover
    (iter (while (< (length next-individuals) (crs-count pop)))
          (for father-idx from 0)
          (for mother-idx = (1+ father-idx))
          (for father = (elt (individuals pop) father-idx))
          (for mother = (elt (individuals pop) mother-idx))
          (for (values left right) = (if (< (random 100) 50) (values father mother) (values mother father)))
          (gen/add (f_% (cross-k (std-env pop) (cons (code left) (env left)) (cons (code right) (env right)))) pop next-individuals))
    ;; mutate crossovers
    (iter (for i from 0 below (mut-crs-count pop))
          (for individual = (elt next-individuals (- (crs-count pop) i 1)))
          (gen/add (f_% (mutate-k (std-env pop) (cons (code individual) (env individual)))) pop next-individuals))
    ;; survivors
    (iter (for i from 0 below (srv-count pop))
          (vector-push-extend (elt (individuals pop) i) next-individuals))
    ;; mutate survivors
    (iter (for i from 0 below (mut-srv-count pop))
          (for individual = (elt (individuals pop) i))
          (gen/add (f_% (mutate-k (std-env pop) (cons (code individual) (env individual)))) pop next-individuals))
     ;; spawn rest
    (iter (while (< (length next-individuals) (pop-size pop)))
          (gen/add (spawn-gen pop) pop next-individuals))
    ;; finish
    (setf (individuals pop) (sort next-individuals #'program<))
    pop))
