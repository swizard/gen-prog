
(in-package :gen-prog)

(defgeneric maybe-infer-type (env expr))
(defmethod maybe-infer-type ((env ty-env) (expr expr))
  (handler-case
      (multiple-value-bind (subst inferred-ty)
          (infer-type env expr)
        (declare (ignore subst))
        inferred-ty)
    (ty-unify-error () nil)
    (ty-var-bind () nil)))

(defgeneric typecheck (env test-expr expected-ty))
(defmethod typecheck ((env ty-env) (test-expr expr) (expected-ty ty))
  (let ((bind (gensym)))
    (maybe-infer-type (make-instance 'ty-env :maps (cons (cons bind (mksc (mkfn expected-ty expected-ty))) (maps env)))
                      (make-instance 'expr-app
                                     :fn (make-instance 'expr-var :name bind)
                                     :arg test-expr))))

(defun run-gen-k (env arg-ty ret-ty &key (depth-lim 4) (set-modif #'identity))
  (let ((arg-bind (gensym)))
    (multiple-value-call
        (curry #'iter-k
               (lambda (body+env k)
                 (values (cons (make-instance 'expr-abs :arg-bind arg-bind :body (car body+env))
                               (cdr body+env))
                         k))
               (constantly nil))
      (gen-k (make-instance 'ty-env :maps (cons (cons arg-bind (mksc arg-ty)) (maps env)))
             ret-ty
             depth-lim
             set-modif
             (constantly nil)))))

(defun gen-k (env expected-ty depth-lim set-modif k)
  (if (zerop depth-lim)
      (funcall k)
      (gen-bindings-k
       env expected-ty depth-lim set-modif
       (f_% (foreach-k
             (lambda (ret-val k) (values (cons (make-instance 'expr-var :name (car ret-val)) env) k))
             (funcall set-modif (remove-if-not (f_ (typecheck env (make-instance 'expr-var :name (car _)) expected-ty)) (maps env)))
             k)))))

(defgeneric curries-count (ty))
(defmethod curries-count ((ty ty)) 0)
(defmethod curries-count ((ty ty-scheme)) (curries-count (ty ty)))
(defmethod curries-count ((ty ty-fn)) (1+ (curries-count (ret ty))))

(defun suitable-applies (env fn-bind)
  (iter (for (arg-bind . arg-ty) in (maps env))
        (for apply-body = (make-instance 'expr-app
                                         :fn (make-instance 'expr-var :name fn-bind)
                                         :arg (make-instance 'expr-var :name arg-bind)))
        (for apply-ty = (maybe-infer-type env apply-body))
        (when apply-ty
          (collect (cons apply-body apply-ty))))) 

(defun gen-bindings-k (env expected-ty depth-lim set-modif k)
  (foreach-k
   (lambda (fn-val fn-k)
     (foreach-k
      (lambda (apply-val apply-k)
        (let ((let-bind (gensym)))
          (multiple-value-call
              (curry #'iter-k
                     (lambda (body+env k)
                       (values (cons (make-instance 'expr-let :let-bind let-bind :let-body (car apply-val) :body (car body+env))
                                     (cdr body+env))
                               k))
                     apply-k)
            (gen-k (make-instance 'ty-env :maps (cons (cons let-bind (mksc (cdr apply-val))) (maps env)))
                   expected-ty
                   (1- depth-lim)
                   set-modif
                   (constantly nil)))))
      (funcall set-modif (suitable-applies env (car fn-val)))
      fn-k))
   (funcall set-modif
            (remove-if-not (f_ (and (ty-applicable _) (< (curries-count _) depth-lim)))
                           (maps env) :key #'cdr))
   k))


