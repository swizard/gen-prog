(defpackage #:gen-prog
  (:use :cl :iterate :metatilities :f-underscore)
  (:shadowing-import-from :metatilities minimize finish make-generator)
  (:export))

(in-package :gen-prog)

(defun shuffle-list (list)
  (iter (with vec = (coerce list 'vector))
        (for i index-of-vector vec)
        (rotatef (elt vec i) (elt vec (random (length vec))))
        (finally (return (coerce vec 'list)))))

(defun swap-remove (vec index)
  (rotatef (elt vec index) (elt vec (1- (length vec))))
  (vector-pop vec))

(defun fold-gen-k (proc seed gen gen-seed k)
  (funcall gen gen-seed
           (lambda (value next-gen-seed)
             (funcall proc value seed (lambda (next-seed) (fold-gen-k proc next-seed gen next-gen-seed k))))
           (f_% (funcall k seed))))

(defun fold-k (proc seed list k)
  (fold-gen-k proc seed
              (lambda (list next finish)
                (if (null list) (funcall finish) (funcall next (car list) (cdr list))))
              list k))

(defun iter-k (action finish value &optional kont)
  (if value
      (funcall action value (f_% (multiple-value-call (curry #'iter-k action finish) (funcall kont))))
      (funcall finish)))

(defun foreach-k (proc list k)
  (fold-k (lambda (value seed k) (funcall proc value (f_% (funcall k seed))))
          nil
          list
          (f_% (funcall k))))

(defun filter-k (proc k)
  (multiple-value-bind (value kont) (funcall k)
    (if (and value kont)
        (let ((next-k (f_% (filter-k proc kont))))
          (funcall proc value (f_ (values _ next-k)) next-k))
        (filter-k proc kont))))
