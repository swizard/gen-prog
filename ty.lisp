
(in-package :gen-prog)

(defclass ty () ())

(defclass ty-bool (ty) ())
(defclass ty-int (ty) ())
(defclass ty-int-nonzero (ty) ())
(defclass ty-float (ty) ())

(defclass ty-fn (ty)
  ((arg :initarg :arg :reader arg)
   (ret :initarg :ret :reader ret)))

(defclass ty-tagged (ty)
  ((tag :initarg :tag :reader tag)))

(defclass ty-template (ty-tagged) ())
(defclass ty-phantom (ty-tagged) ())

(defclass ty-wrapper ()
  ((inner :initarg :inner :type ty :reader inner)))

(defclass ty-upgrade (ty)
  ((ty-base :initarg :base :type ty :reader base)
   (ty-upgrade :initarg :upgrade :type ty :reader upgrade)))

(defclass ty-generic (ty-tagged ty-wrapper) ())

(defmethod arg ((ty ty-generic))
  (assert (ty-functionp ty))
  (let ((inner-ty (arg (inner ty))))
    (if (ty-contains-templatep inner-ty (tag ty))
        (make-instance 'ty-generic :tag (tag ty) :inner inner-ty)
        inner-ty)))

(defmethod ret ((ty ty-generic))
  (assert (ty-functionp ty))
  (let ((inner-ty (ret (inner ty))))
    (if (ty-contains-templatep inner-ty (tag ty))
        (make-instance 'ty-generic :tag (tag ty) :inner inner-ty)
        inner-ty)))

(defmethod bind-generic-ty ((ty ty-generic) (param ty))
  (bind-generic-ty-ty (inner ty) (tag ty) param))
(defmethod bind-generic-ty ((ty ty) (param ty)) ty)

(defmethod bind-generic-ty-ty ((ty ty) tag (param ty)) ty)
(defmethod bind-generic-ty-ty ((ty ty-template) tag (param ty))
  (if (eq tag (tag ty)) param ty))
(defmethod bind-generic-ty-ty ((ty ty-fn) tag (param ty))
  (make-instance 'ty-fn
                 :arg (bind-generic-ty-ty (arg ty) tag param)
                 :ret (bind-generic-ty-ty (ret ty) tag param)))
(defmethod bind-generic-ty-ty ((ty ty-upgrade) tag (param ty))
  (make-instance 'ty-upgrade
                 :base (bind-generic-ty-ty (base ty) tag param)
                 :upgrade (bind-generic-ty-ty (upgrade ty) tag param)))
(defmethod bind-generic-ty-ty ((ty ty-generic) tag (param ty))
  (make-instance 'ty-generic
                 :tag (tag ty)
                 :inner (bind-generic-ty-ty (inner ty) tag param)))

(defmethod typecheck ((ty-a ty) (ty-b ty) &key &allow-other-keys)
  nil)
(defmethod typecheck ((ty-a ty-bool) (ty-b ty-bool) &key generics-a generics-b)
  (values t generics-a generics-b))
(defmethod typecheck ((ty-a ty-int) (ty-b ty-int) &key generics-a generics-b)
  (values t generics-a generics-b))
(defmethod typecheck ((ty-a ty-int-nonzero) (ty-b ty-int-nonzero) &key generics-a generics-b)
  (values t generics-a generics-b))
(defmethod typecheck ((ty-a ty-float) (ty-b ty-float) &key generics-a generics-b)
  (values t generics-a generics-b))
(defmethod typecheck ((ty-a ty-phantom) (ty-b ty-phantom) &key generics-a generics-b)
  (when (eq (tag ty-a) (tag ty-b))
    (values t generics-a generics-b)))
(defmethod typecheck ((ty-a ty-fn) (ty-b ty-fn) &key generics-a generics-b)
  (multiple-value-bind (args-matched-p next-generics-a next-generics-b)
      (typecheck (arg ty-a) (arg ty-b) :generics-a generics-a :generics-b generics-b)
    (when args-matched-p
      (typecheck (ret ty-a) (ret ty-b) :generics-a next-generics-a :generics-b next-generics-b))))

(defmethod typecheck ((ty-a ty-generic) (ty-b ty) &key generics-a generics-b)
  (assert (not (assoc (tag ty-a) generics-a)) nil "generic type ~a already declared" (tag ty-a))
  (typecheck (inner ty-a) ty-b :generics-a (cons (list (tag ty-a)) generics-a) :generics-b generics-b))
(defmethod typecheck ((ty-a ty) (ty-b ty-generic) &key generics-a generics-b)
  (typecheck ty-b ty-a :generics-a generics-b :generics-b generics-a))

(defmethod mkty-ref ((ty ty)) ty)
(defmethod mkty-ref ((ty ty-template)) (make-instance 'ty-phantom :tag (tag ty)))

(defmethod typecheck ((ty-a ty-template) (ty-b ty) &key generics-a generics-b)
  (let ((bind (assoc (tag ty-a) generics-a)))
    (assert bind nil "generic type ~a is not declared" (tag ty-a))
    (if (cdr bind)
        (typecheck (cdr bind) (mkty-ref ty-b) :generics-a generics-a :generics-b generics-b)
        (values t
                (cons (cons (tag ty-a) (mkty-ref ty-b)) (remove (tag ty-a) generics-a :key #'car))
                generics-b))))
(defmethod typecheck ((ty-a ty) (ty-b ty-template) &key generics-a generics-b)
  (typecheck ty-b ty-a :generics-a generics-b :generics-b generics-a))

(defmethod ty-functionp ((ty ty)) nil)
(defmethod ty-functionp ((ty ty-fn)) t)
(defmethod ty-functionp ((ty ty-generic)) (ty-functionp (inner ty)))

(defmethod ty-conditionp ((ty ty)) nil)
(defmethod ty-conditionp ((ty ty-bool)) t)

(defmethod ty-upgradep ((ty ty)) nil)
(defmethod ty-upgradep ((ty ty-upgrade)) t)
(defmethod ty-upgradep ((ty ty-generic)) (ty-upgradep (inner ty)))

(defmethod ty-contains-templatep ((ty ty) tag) nil)
(defmethod ty-contains-templatep ((ty ty-template) tag) (eq tag (tag ty)))
(defmethod ty-contains-templatep ((ty ty-phantom) tag) (eq tag (tag ty)))
(defmethod ty-contains-templatep ((ty ty-fn) tag)
  (or (ty-contains-templatep (arg ty) tag)
      (ty-contains-templatep (ret ty) tag)))
(defmethod ty-contains-templatep ((ty ty-generic) tag)
  (ty-contains-templatep (inner ty) tag))
(defmethod ty-contains-templatep ((ty ty-upgrade) tag)
  (or (ty-contains-templatep (base ty) tag)
      (ty-contains-templatep (upgrade ty) tag)))

(defmacro mkty (type) `(make-instance ',(form-symbol 'ty- type)))
(defmacro mkfn (arg ret) `(make-instance 'ty-fn :arg ,arg :ret ,ret))
(defmacro mkup (base upgrade) `(make-instance 'ty-upgrade :base ,base :upgrade ,upgrade))
(defmacro mktp (tag) `(make-instance 'ty-template :tag ',tag))
(defmacro mkge ((&rest params) inner)
  (labels ((builder (rest-params)
             (if (null rest-params)
                 inner
                 `(make-instance 'ty-generic :tag ',(car rest-params) :inner ,(builder (cdr rest-params))))))
    (builder params)))

(defmethod ty->sexp ((ty ty)) (type-of ty))
(defmethod ty->sexp ((ty ty-fn)) (list (type-of ty) (ty->sexp (arg ty)) '-> (ty->sexp (ret ty))))
(defmethod ty->sexp ((ty ty-generic)) (list (form-symbol '< (tag ty) '>) (ty->sexp (inner ty))))
(defmethod ty->sexp ((ty ty-template)) (tag ty))
(defmethod ty->sexp ((ty ty-phantom)) (form-symbol (tag ty) '*))
(defmethod ty->sexp ((ty ty-upgrade)) (list (type-of ty) (ty->sexp (base ty)) '-> (ty->sexp (upgrade ty))))

